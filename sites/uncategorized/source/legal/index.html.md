---
layout: markdown_page
title: "GitLab Legal"
---
## Legal Information

### Privacy
- [Privacy Policy](https://about.gitlab.com/privacy/) 
- [Privacy Compliance](https://about.gitlab.com/privacy/privacy-compliance/)

### Terms
- [Website Terms of Use](https://about.gitlab.com/terms/#gitlab-com)
- [Subscription Terms](https://about.gitlab.com/terms/#subscription)
- [Professional Services Terms](https://about.gitlab.com/terms/#consultancy)

### Policies
- [DMCA](https://about.gitlab.com/handbook/dmca/)
- [Trademark Use](/handbook/marketing/brand-and-product-marketing/brand/brand-activation/brand-standards/)
- [Code of Business Conduct and Ethics](https://ir.gitlab.com/static-files/7d8c7eb3-cb17-4d68-a607-1b7a1fa1c95d)
